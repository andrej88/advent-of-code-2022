module advent_of_code_2022.day_16.day_16;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.container.binaryheap;
import std.conv;
import std.range;
import std.regex;
import std.string;
import std.typecons;


struct Room
{
    string name;
    int pressurePerMin;
}


string day16Part1(string input)
{
    return solvePart1(parseInput(input), "AA").to!string;
}

string[][Room] parseInput(string input)
{
    return
        input
        .chomp
        .lineSplitter
        .map!((string line) {
            auto captures = line.matchFirst(`^Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? (.+)$`);
            return tuple(
                Room(
                    captures[1],
                    captures[2].to!int,
                ),
                captures[3].split(",").map!(e => e.chompPrefix(" ")).array,
            );
        }).fold!((result, roomAndNeighbours) {
            Room room = roomAndNeighbours[0];
            string[] neighbours = roomAndNeighbours[1];
            result[room] = neighbours;
            return result;
        })(string[][Room].init);
}

@("Parse example input")
unittest
{
    expect(parseInput(
`Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II
`
    )).toEqual([
        Room("AA", 0): ["DD", "II", "BB"],
        Room("BB", 13): ["CC", "AA"],
        Room("CC", 2): ["DD", "BB"],
        Room("DD", 20): ["CC", "AA", "EE"],
        Room("EE", 3): ["FF", "DD"],
        Room("FF", 0): ["EE", "GG"],
        Room("GG", 0): ["FF", "HH"],
        Room("HH", 22): ["GG"],
        Room("II", 0): ["AA", "JJ"],
        Room("JJ", 21): ["II"],
    ]);
}

int solvePart1(string[][Room] network, string startRoom)
{
    Room[] openedBySelf = [getRoomByName(network, startRoom)];
    Room[] openedByOtherActor = []; // dummy

    return getOptimalOption(
        network,
        openedBySelf,
        openedByOtherActor,
        getRoomByName(network, startRoom),
        30,
        0,
        0,
        1,
    );
}

int solvePart2(string[][Room] network, string startRoom)
{
    Room[] openedBySelf = [getRoomByName(network, startRoom)];
    Room[] openedByOtherActor = []; // dummy

    return getOptimalOption(
        network,
        openedBySelf,
        openedByOtherActor,
        getRoomByName(network, startRoom),
        26,
        0,
        0,
        2,
    );
}

int getOptimalOption(
    string[][Room] network,
    ref Room[] openedBySelf,
    ref Room[] openedByOtherActor,
    Room currentRoom,
    int minutesLeft,
    int pressureReleaseSoFar,
    int bestAchievedSoFar,
    int actors,
)
{
    auto allRemainingRooms =
        network
        .keys
        .filter!((Room room) => !openedBySelf.canFind(room) && !openedByOtherActor.canFind(room))
        .map!((Room room) {
            int timeToReachAndOpen = shortestPath(network, currentRoom, room.name) + 1;
            return tuple(
                room,
                timeToReachAndOpen,
                room.pressurePerMin * (minutesLeft - timeToReachAndOpen),
            );
        })
        .filter!((entry) => entry[2] > 0)
        .array
        .sort!((a, b) => a[1] > b[1]);

    if (allRemainingRooms.length == 0)
    {
        debug { import std.stdio : writeln; try { writeln(' '.repeat(30 - minutesLeft).to!string, "Reached bottom of tree: ", openedBySelf, " Total Pressure = ", pressureReleaseSoFar); } catch (Exception) {} }
        return pressureReleaseSoFar;
    }

    // int maxPossible =
    //     pressureReleaseSoFar +
    //     allRemainingRooms.fold!((int acc, room) => acc + room[2])(0);

    // if (maxPossible < bestAchievedSoFar)
    // {
    //     debug { import std.stdio : writeln; try { writeln(' '.repeat(30 - minutesLeft).to!string, "Giving up on branch ", openedBySelf, " with pressureReleasedSoFar = ", pressureReleaseSoFar, " because maxPossible = ", maxPossible, " but bestAchievedSoFar = ", bestAchievedSoFar); } catch (Exception) {} }
    //     return 0;
    // }

    // debug { import std.stdio : writeln; try { writeln("Continuing in branch ", openedBySelf, " with room ", allRemainingRooms); } catch (Exception) {} }

    if (actors == 2)
    {
        int bestOfBranch = 0;

        foreach (roomEntries; selfCartProdNoDups(allRemainingRooms.array))
        {
            Room[] openedByActor1 = openedBySelf.dup;
            Room[] openedByActor2 = [];

            Room actor1NextRoom = roomEntries[0][0];
            int actor1TimeToReachAndOpenRoom = roomEntries[0][1];
            int actor1pressureRelease = roomEntries[0][2];

            Room actor2NextRoom = roomEntries[1][0];
            int actor2TimeToReachAndOpenRoom = roomEntries[1][1];
            int actor2pressureRelease = roomEntries[1][2];

            import std.stdio : write, writeln;
            writeln(' '.repeat(30 - minutesLeft).to!string, "Opening ", actor1NextRoom.name, " and ", actor2NextRoom.name);

            int currentResult = 0;

            currentResult += getOptimalOption(
                network,
                openedByActor1,
                openedByActor2,
                actor1NextRoom,
                minutesLeft - actor1TimeToReachAndOpenRoom,
                pressureReleaseSoFar + actor1pressureRelease,
                max(bestAchievedSoFar, bestOfBranch),
                1,                                      // Recursive calls always have one actor. They don't multiply at each valve :)
            );

            currentResult += getOptimalOption(
                network,
                openedByActor2,
                openedByActor1,
                actor2NextRoom,
                minutesLeft - actor2TimeToReachAndOpenRoom,
                pressureReleaseSoFar + actor2pressureRelease,
                max(bestAchievedSoFar, bestOfBranch),                      // TODO is this right?
                1,                                      // Recursive calls always have one actor. They don't multiply at each valve :)
            );

            writeln(' '.repeat(30 - minutesLeft).to!string, "Opened ", actor1NextRoom.name, " and ", actor2NextRoom.name, ", got ", currentResult);

            if (currentResult > bestOfBranch)
            {
                bestOfBranch = currentResult;
            }
        }

        return bestOfBranch;
    }
    else if (actors == 1)
    {
        Room[] openedBySelfSoFar = openedBySelf.dup;
        int bestOfBranch = 0;

        foreach (Tuple!(Room, int, int) entry; allRemainingRooms)
        {
            Room nextRoom = entry[0];
            int timeToReachAndOpenRoom = entry[1];
            int pressureRelease = entry[2];

            Room[] openedBySelfWithNext = openedBySelfSoFar ~ nextRoom;

            import std.stdio : write, writeln;
            writeln(' '.repeat(30 - minutesLeft).to!string, "Opening ", nextRoom.name);

            int currentResult = getOptimalOption(
                network,
                openedBySelfWithNext,
                openedByOtherActor,                     // dummy
                nextRoom,
                minutesLeft - timeToReachAndOpenRoom,
                pressureReleaseSoFar + pressureRelease,
                max(bestAchievedSoFar, bestOfBranch),
                1,                                      // Recursive calls always have one actor. They don't multiply at each valve :)
            );

            writeln(' '.repeat(30 - minutesLeft).to!string, "Opened ", nextRoom.name, ", got ", currentResult);

            if (currentResult > bestOfBranch)
            {
                bestOfBranch = currentResult;
            }
        }

        return bestOfBranch;
    }
    else
    {
        assert(false, "Invalid number of actors: " ~ actors.to!string);
    }
}

@("Example Part 1")
unittest
{
    expect(solvePart1([
        Room("AA", 0): ["DD", "II", "BB"],
        Room("BB", 13): ["CC", "AA"],
        Room("CC", 2): ["DD", "BB"],
        Room("DD", 20): ["CC", "AA", "EE"],
        Room("EE", 3): ["FF", "DD"],
        Room("FF", 0): ["EE", "GG"],
        Room("GG", 0): ["FF", "HH"],
        Room("HH", 22): ["GG"],
        Room("II", 0): ["AA", "JJ"],
        Room("JJ", 21): ["II"],
    ], "AA")).toEqual(1651);
}

@("Example Part 2")
unittest
{
    expect(solvePart2([
        Room("AA", 0): ["DD", "II", "BB"],
        Room("BB", 13): ["CC", "AA"],
        Room("CC", 2): ["DD", "BB"],
        Room("DD", 20): ["CC", "AA", "EE"],
        Room("EE", 3): ["FF", "DD"],
        Room("FF", 0): ["EE", "GG"],
        Room("GG", 0): ["FF", "HH"],
        Room("HH", 22): ["GG"],
        Room("II", 0): ["AA", "JJ"],
        Room("JJ", 21): ["II"],
    ], "AA")).toEqual(1707);
}

int[string][Room][string[][Room]] shortestPathCache;

int shortestPath(string[][Room] network, Room from, string to)
{
    if (from.name == to) return 0;

    if (
        network in shortestPathCache &&
        from in shortestPathCache[network] &&
        to in shortestPathCache[network][from]
    )
    {
        return shortestPathCache[network][from][to];
    }

    string[] alreadyVisited;
    Tuple!(string, int)[] toVisit =
        network[from]
        .map!((string roomName) => tuple(roomName, 1))
        .array;

    while (toVisit.length > 0)
    {
        string roomName = toVisit[0][0];
        int distance = toVisit[0][1];
        toVisit = toVisit[1..$];

        if (roomName == to)
        {
            shortestPathCache[network][from][to] = distance;
            return distance;
        }
        else
        {
            toVisit ~=
                network[getRoomByName(network, roomName)]
                .filter!((string neighbour) => !alreadyVisited.canFind(neighbour))
                .map!((string neighbour) => tuple(neighbour, distance + 1))
                .array;
        }
    }

    assert(false, "No path found from " ~ from.name ~ " to " ~ to ~ ".");
}

Room getRoomByName(string[][Room] network, string name)
{
    foreach (Room room; network.keys)
    {
        if (room.name == name) return room;
    }

    assert(false, "Could not find room named " ~ name ~ " in network " ~ network.to!string);
}

@("shortestPath")
unittest
{
    expect(shortestPath(
        [
            Room("AA", 0): ["DD", "II", "BB"],
            Room("BB", 13): ["CC", "AA"],
            Room("CC", 2): ["DD", "BB"],
            Room("DD", 20): ["CC", "AA", "EE"],
            Room("EE", 3): ["FF", "DD"],
            Room("FF", 0): ["EE", "GG"],
            Room("GG", 0): ["FF", "HH"],
            Room("HH", 22): ["GG"],
            Room("II", 0): ["AA", "JJ"],
            Room("JJ", 21): ["II"],
        ],
        Room("II", 0),
        "DD",
    )).toEqual(2);

    expect(shortestPath(
        [
            Room("AA", 0): ["DD", "II", "BB"],
            Room("BB", 13): ["CC", "AA"],
            Room("CC", 2): ["DD", "BB"],
            Room("DD", 20): ["CC", "AA", "EE"],
            Room("EE", 3): ["FF", "DD"],
            Room("FF", 0): ["EE", "GG"],
            Room("GG", 0): ["FF", "HH"],
            Room("HH", 22): ["GG"],
            Room("II", 0): ["AA", "JJ"],
            Room("JJ", 21): ["II"],
        ],
        Room("HH", 22),
        "JJ",
    )).toEqual(7);
}

/// Returns the cartesian product of the array with itself, but without entries
/// that are duplicates when ignoring order, and without any elements paired
/// with themselves.
Tuple!(T, T)[] selfCartProdNoDups(T)(T[] arr)
{
    Tuple!(T, T)[] result;

    foreach (size_t i; 0 .. arr.length)
    {
        foreach (size_t j; i + 1 .. arr.length)
        {
            result ~= tuple(arr[i], arr[j]);
        }
    }

    return result;
}

@("selfCartProdNoDups")
unittest
{
    expect(selfCartProdNoDups(["A", "B", "C", "D"])).toEqual([
        tuple("A", "B"),
        tuple("A", "C"),
        tuple("A", "D"),
        tuple("B", "C"),
        tuple("B", "D"),
        tuple("C", "D"),
    ]);
}
