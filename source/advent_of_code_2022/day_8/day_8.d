module advent_of_code_2022.day_8.day_8;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.string;
import std.utf;


string day8Part1(string input)
{
    return
        input
        .parseInput
        .countVisibleTrees
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day8Part1(
`30373
25512
65332
33549
35390
`
    )).toEqual("21");
}

string day8Part2(string input)
{
    int[][] trees = input.parseInput;
    size_t maxScenicScore = 0;

    foreach (size_t y, int[] row; trees)
    {
        foreach (size_t x, int _treeHeight; row)
        {
            maxScenicScore = max(maxScenicScore, calcScenicScore(trees, x, y));
        }
    }

    return maxScenicScore.to!string;
}

@("Example Part 1")
unittest
{
    expect(day8Part2(
`30373
25512
65332
33549
35390
`
    )).toEqual("8");
}

int[][] parseInput(string input)
out(result; result.all!((row) => row.length == result.length))
{
    return
        input
        .chomp
        .lineSplitter
        .map!(
            (string line) =>
                line
                .byChar
                .map!((char c) => c.to!string.to!int)
                .array
        )
        .array;
}

@("Parse example input")
unittest
{
    expect(parseInput(
`30373
25512
65332
33549
35390
`
    )).toEqual([
        [3, 0, 3, 7, 3],
        [2, 5, 5, 1, 2],
        [6, 5, 3, 3, 2],
        [3, 3, 5, 4, 9],
        [3, 5, 3, 9, 0],
    ]);
}

size_t countVisibleTrees(int[][] trees)
in(trees.all!((row) => row.length == trees.length))
{
    size_t result;

    foreach (size_t y, int[] row; trees)
    {
        foreach (size_t x, int treeHeight; row)
        {
            result += isTreeVisible(trees, x, y);
        }
    }

    return result;
}

@("Count visible trees in example")
unittest
{
    expect(countVisibleTrees([
        [3, 0, 3, 7, 3],
        [2, 5, 5, 1, 2],
        [6, 5, 3, 3, 2],
        [3, 3, 5, 4, 9],
        [3, 5, 3, 9, 0],
    ])).toEqual(21);
}

bool isTreeVisible(int[][] trees, size_t x, size_t y)
in(trees.all!((row) => row.length == trees.length))
{
    int[] theRow = trees[y];
    int[] theColumn = trees.map!((int[] row) => row[x]).array;
    int thisTreeHeight = trees[y][x];

    return
        theRow[0   .. x].all!(t => t < thisTreeHeight) ||
        theRow[x+1 .. $].all!(t => t < thisTreeHeight) ||
        theColumn[0   .. y].all!(t => t < thisTreeHeight) ||
        theColumn[y+1 .. $].all!(t => t < thisTreeHeight);
}

@("Determine if a tree is visible")
unittest
{
    int[][] trees = [
        [3, 0, 3, 7, 3],
        [2, 5, 5, 1, 2],
        [6, 5, 3, 3, 2],
        [3, 3, 5, 4, 9],
        [3, 5, 3, 9, 0],
    ];

    // Middle 3 in row 2
    expect(isTreeVisible(trees, 1, 1)).toEqual(true);   // 5
    expect(isTreeVisible(trees, 2, 1)).toEqual(true);   // 5
    expect(isTreeVisible(trees, 3, 1)).toEqual(false);  // 1

    // Middle 3 in row 3
    expect(isTreeVisible(trees, 1, 2)).toEqual(true);   // 5
    expect(isTreeVisible(trees, 2, 2)).toEqual(false);  // 3
    expect(isTreeVisible(trees, 3, 2)).toEqual(true);   // 3

    // Middle 3 in row 4
    expect(isTreeVisible(trees, 1, 3)).toEqual(false);  // 3
    expect(isTreeVisible(trees, 2, 3)).toEqual(true);   // 5
    expect(isTreeVisible(trees, 3, 3)).toEqual(false);  // 4
}

size_t calcScenicScore(int[][] trees, size_t x, size_t y)
in(trees.all!((row) => row.length == trees.length))
{
    int[] theRow = trees[y];
    int[] theColumn = trees.map!((int[] row) => row[x]).array;
    int thisTreeHeight = trees[y][x];

    return
        theRow[0 .. x].retro.until!((int height) => height >= thisTreeHeight)(No.openRight).walkLength *
        theRow[x + 1 .. $].until!((int height) => height >= thisTreeHeight)(No.openRight).walkLength *
        theColumn[0 .. y].retro.until!((int height) => height >= thisTreeHeight)(No.openRight).walkLength *
        theColumn[y + 1 .. $].until!((int height) => height >= thisTreeHeight)(No.openRight).walkLength;
}

@("Determine if a tree is visible")
unittest
{
    int[][] trees = [
        [3, 0, 3, 7, 3],
        [2, 5, 5, 1, 2],
        [6, 5, 3, 3, 2],
        [3, 3, 5, 4, 9],
        [3, 5, 3, 9, 0],
    ];

    // Edges should all be zero
    foreach (size_t y, int[] row; trees)
    {
        foreach (size_t x, int _treeHeight; row)
        {
            if (y != 0 && x != 0) break;
            expect(calcScenicScore(trees, x, y)).toEqual(0);
        }
    }

    expect(calcScenicScore(trees, 2, 1)).toEqual(4);    // Middle 5 in second row
    expect(calcScenicScore(trees, 2, 3)).toEqual(8);    // Middle 5 in fourth row
}
