module advent_of_code_2022.day_test;

import exceeds_expectations;
import std.conv : to;


string dayTest(string input)
{
    return input.length.to!string;
}

@("returns the length of the string")
unittest
{
    expect(dayTest("hello")).toEqual("5");
}
