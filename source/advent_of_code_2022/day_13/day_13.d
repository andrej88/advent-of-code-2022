module advent_of_code_2022.day_13.day_13;

import advent_of_code_2022.util;
import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.path;
import std.range;
import std.regex;
import std.string;
import std.sumtype;


version(unittest) enum string exampleInput = import(__FILE__.pathSplitter.array[1 .. $-1].join("/").buildPath("example.txt"));

alias PacketNode = SumType!(int, This[]);


string day13Part1(string input)
{
    return
        input
        .parseInput
        .map!((PacketNode[2] pair) => arePacketsInOrder(pair[0], pair[1]))
        .enumerate(1)
        .filter!((entry) => entry[1])
        .map!((entry) => entry[0])
        .sum
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day13Part1(exampleInput)).toEqual("13");
}

string stringify(RangeOfPacketNode)(RangeOfPacketNode packetNodes)
if (isInputRange!RangeOfPacketNode)
{
    return packetNodes.map!stringify.joiner("\n").to!string;
}

string stringify(PacketNode packetNode)
{
    return packetNode.match!(
        (int i) => i.to!string,
        (PacketNode[] packets) => "[" ~ packets.map!stringify.joiner(",").to!string ~ "]",
    );
}

string day13Part2(string input)
{
    PacketNode[] dividerPackets = [
        PacketNode([ PacketNode([ PacketNode(2) ]) ]),
        PacketNode([ PacketNode([ PacketNode(6) ]) ]),
    ];

    return
        input
        .parseInput
        .map!(to!(PacketNode[]))
        .joiner
        .chain(dividerPackets)
        .array
        .sort!arePacketsInOrder
        // .inspect!stringify
        .enumerate(1)
        .filter!((entry) => entry[1] == dividerPackets[0] || entry[1] == dividerPackets[1])    // Using canFind produces a linker error here LOL
        .map!((entry) => entry[0])
        // .inspect!stringify
        .fold!((acc, e) => acc * e)
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day13Part2(exampleInput)).toEqual("140");
}

PacketNode[2][] parseInput(string input)
{
    return
        input
        .split("\n\n")
        .map!((string packets) {
            string[] packetsLines = packets.lineSplitter.array;
            assert(
                packetsLines.length >= 2,
                "Not enough lines in packet pair. Need 2 but got " ~ packetsLines.length.to!string ~ ": \"" ~
                packetsLines.to!string ~ "\""
            );
            return cast(PacketNode[2])[
                parsePacket(packetsLines[0]),
                parsePacket(packetsLines[1]),
            ];
        })
        .array;
}

PacketNode parsePacket(string packetString)
{
    if (auto captures = packetString.matchFirst(`^\d+$`))
    {
        return PacketNode(captures[0].to!int);
    }
    else if (auto captures = packetString.matchFirst(`^\[(.*)\]$`))
    {
        string arrayContents = captures[1];
        if (arrayContents.length == 0)
            return PacketNode([]);

        PacketNode[] subPackets;

        size_t consumedChars;
        while (consumedChars < arrayContents.length)
        {
            string item;
            int nesting;

            foreach (char c; arrayContents[consumedChars..$])
            {
                consumedChars++;
                if (c == ',' && nesting == 0) break;
                if (c == '[') nesting++;
                if (c == ']') nesting--;
                item ~= c;
            }

            subPackets ~= parsePacket(item);
        }

        return PacketNode(subPackets);
    }
    else assert(
        false,
        "Given packet was expected to be either a standalone number or an array, but got: \"" ~ packetString ~ "\""
    );
}

@("Parse example input")
unittest
{
    expect(parseInput(exampleInput)).toEqual([
        [
            PacketNode([ PacketNode(1), PacketNode(1), PacketNode(3), PacketNode(1), PacketNode(1) ]),
            PacketNode([ PacketNode(1), PacketNode(1), PacketNode(5), PacketNode(1), PacketNode(1) ]),
        ],
        [
            PacketNode([
                PacketNode([ PacketNode(1) ]),
                PacketNode([ PacketNode(2), PacketNode(3), PacketNode(4) ]),
            ]),
            PacketNode([
                PacketNode([ PacketNode(1) ]),
                PacketNode(4),
            ]),
        ],
        [
            PacketNode([ PacketNode(9) ]),
            PacketNode([ PacketNode([ PacketNode(8), PacketNode(7), PacketNode(6) ]) ]),
        ],
        [
            PacketNode([
                PacketNode([ PacketNode(4), PacketNode(4) ]),
                PacketNode(4),
                PacketNode(4),
            ]),
            PacketNode([
                PacketNode([ PacketNode(4), PacketNode(4) ]),
                PacketNode(4),
                PacketNode(4),
                PacketNode(4),
            ]),
        ],
        [
            PacketNode([ PacketNode(7), PacketNode(7), PacketNode(7), PacketNode(7) ]),
            PacketNode([ PacketNode(7), PacketNode(7), PacketNode(7) ]),
        ],
        [
            PacketNode([]),
            PacketNode([ PacketNode(3) ]),
        ],
        [
            PacketNode([ PacketNode([ PacketNode([]) ]) ]),
            PacketNode([ PacketNode([]) ]),
        ],
        [
            PacketNode([
                PacketNode(1),
                PacketNode([
                    PacketNode(2),
                    PacketNode([
                        PacketNode(3),
                        PacketNode([
                            PacketNode(4),
                            PacketNode([ PacketNode(5), PacketNode(6), PacketNode(7) ]),
                        ]),
                    ]),
                ]),
                PacketNode(8),
                PacketNode(9),
            ]),
            PacketNode([
                PacketNode(1),
                PacketNode([
                    PacketNode(2),
                    PacketNode([
                        PacketNode(3),
                        PacketNode([
                            PacketNode(4),
                            PacketNode([ PacketNode(5), PacketNode(6), PacketNode(0) ]),
                        ]),
                    ]),
                ]),
                PacketNode(8),
                PacketNode(9),
            ]),
        ]
    ]);
}

/// Returns:
///     left <  right  -> -1
///     left == right  ->  0
///     left >  right  ->  1
int comparePackets(PacketNode left, PacketNode right)
out(result; result == -1 || result == 0 || result == 1)
{
    return left.match!(
        (int iLeft) => right.match!(
            (int iRight) =>
                iLeft < iRight ?
                -1 :
                iLeft == iRight ?
                0 :
                1,
            (PacketNode[] _nodesRight) => comparePackets(PacketNode([ left ]), right),
        ),
        (PacketNode[] nodesLeft) => right.match!(
            (int _iLeft) => comparePackets(left, PacketNode([ right ])),
            (PacketNode[] nodesRight) {
                foreach (size_t i; 0..nodesRight.length)
                {
                    if (i >= nodesLeft.length) return -1;

                    int leftVsRight = comparePackets(nodesLeft[i], nodesRight[i]);
                    if (leftVsRight == 0) continue;

                    return leftVsRight;
                }

                if (nodesRight.length < nodesLeft.length)
                    return 1;
                else
                    return 0;
            }
        ),
    );
}

bool arePacketsInOrder(PacketNode left, PacketNode right)
{
    // TODO: What if they're identical?
    return comparePackets(left, right) == -1;
}

@("comparePackets example")
unittest
{
    expect(arePacketsInOrder(
        PacketNode([ PacketNode(1), PacketNode(1), PacketNode(3), PacketNode(1), PacketNode(1) ]),
        PacketNode([ PacketNode(1), PacketNode(1), PacketNode(5), PacketNode(1), PacketNode(1) ]),
    )).toEqual(true);

    expect(arePacketsInOrder(
        PacketNode([
            PacketNode([ PacketNode(1) ]),
            PacketNode([ PacketNode(2), PacketNode(3), PacketNode(4) ]),
        ]),
        PacketNode([
            PacketNode([ PacketNode(1) ]),
            PacketNode(4),
        ]),
    )).toEqual(true);

    expect(arePacketsInOrder(
        PacketNode([ PacketNode(9) ]),
        PacketNode([ PacketNode([ PacketNode(8), PacketNode(7), PacketNode(6) ]) ]),
    )).toEqual(false);

    expect(arePacketsInOrder(
        PacketNode([
            PacketNode([ PacketNode(4), PacketNode(4) ]),
            PacketNode(4),
            PacketNode(4),
        ]),
        PacketNode([
            PacketNode([ PacketNode(4), PacketNode(4) ]),
            PacketNode(4),
            PacketNode(4),
            PacketNode(4),
        ]),
    )).toEqual(true);

    expect(arePacketsInOrder(
        PacketNode([ PacketNode(7), PacketNode(7), PacketNode(7), PacketNode(7) ]),
        PacketNode([ PacketNode(7), PacketNode(7), PacketNode(7) ]),
    )).toEqual(false);

    expect(arePacketsInOrder(
        PacketNode([]),
        PacketNode([ PacketNode(3) ]),
    )).toEqual(true);

    expect(arePacketsInOrder(
        PacketNode([ PacketNode([ PacketNode([]) ]) ]),
        PacketNode([ PacketNode([]) ]),
    )).toEqual(false);

    expect(arePacketsInOrder(
        PacketNode([
            PacketNode(1),
            PacketNode([
                PacketNode(2),
                PacketNode([
                    PacketNode(3),
                    PacketNode([
                        PacketNode(4),
                        PacketNode([ PacketNode(5), PacketNode(6), PacketNode(7) ]),
                    ]),
                ]),
            ]),
            PacketNode(8),
            PacketNode(9),
        ]),
        PacketNode([
            PacketNode(1),
            PacketNode([
                PacketNode(2),
                PacketNode([
                    PacketNode(3),
                    PacketNode([
                        PacketNode(4),
                        PacketNode([ PacketNode(5), PacketNode(6), PacketNode(0) ]),
                    ]),
                ]),
            ]),
            PacketNode(8),
            PacketNode(9),
        ]),
    )).toEqual(false);
}
