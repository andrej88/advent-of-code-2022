module advent_of_code_2022.day_14.day_14;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.string;
import std.typecons;


struct Vector2
{
    int x;
    int y;
}

enum Vector2 sandSourceAt500_0 = Vector2(500, 0);


string day14Part1(string input)
{
    return simulateThenCountSand(
        input,
        Yes.stopWhenOutOfBounds,
        No.addFloor,
    );
}

@("Example Part 1")
unittest
{
    expect(day14Part1(
`498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
`
    )).toEqual("24");
}

string day14Part2(string input)
{
    return simulateThenCountSand(
        input,
        No.stopWhenOutOfBounds,
        Yes.addFloor,
    );
}

@("Example Part 2")
unittest
{
    expect(day14Part2(
`498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
`
    )).toEqual("93");
}

Vector2[][] parseInput(string input)
{
    return
        input
        .chomp
        .lineSplitter
        .map!(
            (string line) =>
                line
                .splitter(" -> ")
                .map!((string coords) {
                    string[] coordParts = coords.split(",");
                    return Vector2(coordParts[0].to!int, coordParts[1].to!int);
                })
                .array
        )
        .array;
}

string simulateThenCountSand(
    string input,
    Flag!"stopWhenOutOfBounds" stopWhenOutOfBounds,
    Flag!"addFloor" addFloor,
)
{
    ubyte[][] world = input.parseInput.createWorld(sandSourceAt500_0);

    if (addFloor)
    {
        world ~= ubyte(' ').repeat(world[0].length).array;
        world ~= ubyte('#').repeat(world[0].length).array;
    }

    // Just for fun:
    debug import std.stdio : writeln;
    debug writeln(stringifyWorld(world));

    simulateUntilEquilibrium(world, stopWhenOutOfBounds);

    debug writeln(stringifyWorld(world));

    return countSand(world).to!string;
}

@("Parse example input")
unittest
{
    expect(parseInput(
`498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
`
    )).toEqual([
        [ Vector2(498, 4), Vector2(498, 6), Vector2(496, 6) ],
        [ Vector2(503, 4), Vector2(502, 4), Vector2(502, 9), Vector2(494, 9) ],
    ]);
}

// Return the top-left and bottom-right vectors of the smallest square
// containing all the given points. Both corners are on the "inside" of the
// square.
Tuple!(Vector2, Vector2) getBoundingBox(RangeOfVector2)(RangeOfVector2 points)
if (isInputRange!RangeOfVector2 && is(ElementType!RangeOfVector2 == Vector2))
{
    return tuple(
        Vector2(
            points.minElement!((Vector2 v) => v.x).x,
            points.minElement!((Vector2 v) => v.y).y,
        ),
        Vector2(
            points.maxElement!((Vector2 v) => v.x).x,
            points.maxElement!((Vector2 v) => v.y).y,
        ),
    );
}

@("getBoundingBox example")
unittest
{
    expect(getBoundingBox(
        sandSourceAt500_0 ~ [
            [ Vector2(498, 4), Vector2(498, 6), Vector2(496, 6) ],
            [ Vector2(503, 4), Vector2(502, 4), Vector2(502, 9), Vector2(494, 9) ],
        ].join
    )).toEqual(tuple(Vector2(494, 0), Vector2(503, 9)));
}

ubyte[][] createWorld(Vector2[][] rockFormations, Vector2 sandSource)
{
    Tuple!(Vector2, Vector2) box = getBoundingBox(sandSource ~ rockFormations.join);
    Vector2 topLeft = box[0];

    Vector2 dimensions = Vector2(
        box[1].x - topLeft.x + 1,
        box[1].y - topLeft.y + 1,
    );

    ubyte[][] world;
    world.length = dimensions.y;
    foreach (ref ubyte[] row; world)
    {
        row.length = dimensions.x;
        row[] = ubyte(' ');
    }

    world[sandSource.y - topLeft.y][sandSource.x - topLeft.x] = '+';

    foreach (Vector2[] rockFormation; rockFormations)
    {
        foreach (segment; rockFormation.slide(2))
        {
            if (segment[0].x == segment[1].x)
            {
                // Vertical
                int x = segment[0].x;
                int direction = segment[1].y > segment[0].y ? 1 : -1;
                foreach (int y; iota(segment[0].y, segment[1].y + direction, direction))
                {
                    world[(y - topLeft.y).to!size_t][(x - topLeft.x).to!size_t] = '#';
                }
            }
            else if (segment[0].y == segment[1].y)
            {
                // Horizontal
                int y = segment[0].y;
                int direction = segment[1].x > segment[0].x ? 1 : -1;
                foreach (int x; iota(segment[0].x, segment[1].x + direction, direction))
                {
                    world[(y - topLeft.y).to!size_t][(x - topLeft.x).to!size_t] = '#';
                }
            }
            else
            {
                // Shouldn't happen
                assert(
                    false,
                    "Invalid segment is not horizontal or vertical: " ~
                    segment[0].to!string ~ " to " ~ segment[1].to!string
                );
            }
        }
    }

    return world;
}

@("createWorld example")
unittest
{
    expect(createWorld(
        [
            [ Vector2(498, 4), Vector2(498, 6), Vector2(496, 6) ],
            [ Vector2(503, 4), Vector2(502, 4), Vector2(502, 9), Vector2(494, 9) ],
        ],
        sandSourceAt500_0,
    )).toEqual([
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', '+', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#'],
        cast(ubyte[])[' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', '#', '#', '#', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])['#', '#', '#', '#', '#', '#', '#', '#', '#', ' '],
    ]);
}

void simulateUntilEquilibrium(
    ref ubyte[][] world,
    Flag!"stopWhenOutOfBounds" stopWhenOutOfBounds,
)
{
    Vector2 sandSource;
    foreach (size_t y, const ubyte[] row; world)
    {
        foreach (size_t x, ubyte cell; row)
        {
            if (cell == '+')
            {
                sandSource = Vector2(x.to!int, y.to!int);
            }
        }
    }

    simulation:
    while (true)
    {
        // debug { import std.stdio : writeln; try { writeln("Unit " ~ unit.to!string); } catch (Exception) {} }
        Vector2 currentSand = sandSource;

        step:
        while (true)
        {
            // debug { import std.stdio : writeln; try { writeln(currentSand); } catch (Exception) {} }

            if (currentSand.y + 1 >= world.length)
            {
                break simulation;
            }
            else if (world[currentSand.y + 1][currentSand.x] == ' ')
            {
                currentSand.y += 1;
            }

            else if (currentSand.x - 1 < 0)
            {
                if (stopWhenOutOfBounds) break simulation;

                foreach (ref ubyte[] row; world)
                {
                    row.insertInPlace(0, ' ');
                }

                world[$ - 1][0] = '#';
                currentSand.x += 1;
                sandSource.x += 1;
            }
            else if (world[currentSand.y + 1][currentSand.x - 1] == ' ')
            {
                currentSand.y += 1;
                currentSand.x -= 1;
            }

            else if (currentSand.x + 1 >= world[0].length)
            {
                if (stopWhenOutOfBounds) break simulation;

                foreach (ref ubyte[] row; world)
                {
                    row.length += 1;
                    row[$ - 1] = ' ';
                }

                world[$ - 1][$ - 1] = '#';
            }
            else if (world[currentSand.y + 1][currentSand.x + 1] == ' ')
            {
                currentSand.y += 1;
                currentSand.x += 1;
            }

            else if (world[currentSand.y][currentSand.x] == '+')
            {
                world[currentSand.y][currentSand.x] = 'O';
                break simulation;
            }

            else
            {
                world[currentSand.y][currentSand.x] = 'O';
                // debug { import std.stdio : writeln; try { writeln(stringifyWorld(world)); } catch (Exception) {} }
                break step;
            }
        }
    }
}

@("simulateUntilEquilibrium example")
unittest
{
    ubyte[][] world = [
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', '+', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#'],
        cast(ubyte[])[' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', '#', '#', '#', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' '],
        cast(ubyte[])['#', '#', '#', '#', '#', '#', '#', '#', '#', ' '],
    ];

    simulateUntilEquilibrium(world, Yes.stopWhenOutOfBounds);

    expect(stringifyWorld(world)).toEqual(
        "      +   \n" ~
        "          \n" ~
        "      O   \n" ~
        "     OOO  \n" ~
        "    #OOO##\n" ~
        "   O#OOO# \n" ~
        "  ###OOO# \n" ~
        "    OOOO# \n" ~
        " O OOOOO# \n" ~
        "######### \n"
    );
}

string stringifyWorld(const ubyte[][] world)
{
    return world.map!((const ubyte[] row) => row.map!(to!char).to!string).join("\n") ~ "\n";
}

int countSand(in ubyte[][] world)
{
    return
        world
        .map!((const ubyte[] row) => row.count('O'))
        .sum
        .to!int;
}

@("countSand example")
unittest
{
    expect(countSand([
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', '+', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', ' ', 'O', ' ', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', ' ', 'O', 'O', 'O', ' ', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', '#', 'O', 'O', 'O', '#', '#'],
        cast(ubyte[])[' ', ' ', ' ', 'O', '#', 'O', 'O', 'O', '#', ' '],
        cast(ubyte[])[' ', ' ', '#', '#', '#', 'O', 'O', 'O', '#', ' '],
        cast(ubyte[])[' ', ' ', ' ', ' ', 'O', 'O', 'O', 'O', '#', ' '],
        cast(ubyte[])[' ', 'O', ' ', 'O', 'O', 'O', 'O', 'O', '#', ' '],
        cast(ubyte[])['#', '#', '#', '#', '#', '#', '#', '#', '#', ' '],
    ])).toEqual(24);
}
