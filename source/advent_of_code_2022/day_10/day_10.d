module advent_of_code_2022.day_10.day_10;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.string;
import std.sumtype;


string day10Part1(string input)
{
    return
        input
        .parseInput
        .evaluateAndGetXRegOverTime
        .enumerate(1)
        .map!((entry) => entry.index * entry.value)
        .drop(19)
        .stride(40)
        .sum
        .to!string;
}

version(unittest) enum string exampleProgram = import("advent_of_code_2022/day_10/example.txt");

@("Example Part 1")
unittest
{
    expect(day10Part1(exampleProgram)).toEqual("13140");
}

string day10Part2(string input)
{
    return
        input
        .parseInput
        .evaluateAndGetXRegOverTime
        .enumerate(0)
        .map!((entry) {
            int pixelBeingDrawn = entry.index % 40;
            return (
                pixelBeingDrawn == entry.value - 1 ||
                pixelBeingDrawn == entry.value ||
                pixelBeingDrawn == entry.value + 1
            ) ? '█' : '░';

        })
        .chunks(40)
        .join("\n")
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day10Part2(exampleProgram)).toEqual(
`██░░██░░██░░██░░██░░██░░██░░██░░██░░██░░
███░░░███░░░███░░░███░░░███░░░███░░░███░
████░░░░████░░░░████░░░░████░░░░████░░░░
█████░░░░░█████░░░░░█████░░░░░█████░░░░░
██████░░░░░░██████░░░░░░██████░░░░░░████
███████░░░░░░░███████░░░░░░░███████░░░░░`
    );
}

struct Noop {}
struct AddX { int value; }
alias Instr = SumType!(Noop, AddX);

Instr[] parseInput(string input)
{
    return
        input
        .chomp
        .lineSplitter
        .map!((string line) {
            if (line == "noop")
                return Noop().to!Instr;
            else
                return AddX(line[5..$].to!int).to!Instr;
        })
        .array;
}

@("Parse small example")
unittest
{
    expect(parseInput(
`noop
addx 3
addx -5
`
    )).toEqual([
        Noop().to!Instr,
        AddX(3).to!Instr,
        AddX(-5).to!Instr,
    ]);
}

// Evaluate the given program and return an array containing the value of
// register X during each cycle. The first cycle is at index 0, not 1.
int[] evaluateAndGetXRegOverTime(Instr[] program)
{
    enum int registerXInitialValue = 1;

    int[] result;
    int registerX = registerXInitialValue;

    foreach (Instr instr; program)
    {
        instr.match!(
            (Noop _noop) { result ~= registerX; },
            (AddX addX) {
                // two cycles and then it changes:
                result ~= registerX;
                result ~= registerX;
                registerX += addX.value;
            },
        );
    }

    return result;
}

@("Evaluate small example")
unittest
{
    expect(evaluateAndGetXRegOverTime([
        Noop().to!Instr,
        AddX(3).to!Instr,
        AddX(-5).to!Instr,
    ])).toEqual([1, 1, 1, 4, 4]); // There is no 6th cycle but if there were, X would be -1.
}
