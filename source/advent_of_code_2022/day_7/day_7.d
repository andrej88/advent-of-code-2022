module advent_of_code_2022.day_7.day_7;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.regex;
import std.string;
import std.sumtype;
import std.traits;
import std.typecons;


string day7Part1(string input)
{
    return
        input
        .tokenize
        .interpret
        .getSumOfDirectoriesLessThan!100_000
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day7Part1(
`$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
`
    )).toEqual("95437");
}

string day7Part2(string input)
{
    Directory rootDir =
        input
        .tokenize
        .interpret;

    enum size_t diskSize = 70_000_000;
    enum size_t spaceNeeded = 30_000_000;
    enum size_t maxAllowedCapacity = diskSize - spaceNeeded;
    size_t currentSpaceUsed = calcDirectoryFileSize(rootDir);

    // This assumes the disk *is* too full. If not, there will be an underflow and everything will break.
    size_t minimumToDelete = currentSpaceUsed - maxAllowedCapacity;

    return
        .getSmallestDirectoryGreaterThan(minimumToDelete, rootDir)
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day7Part2(
`$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
`
    )).toEqual("24933642");
}

struct TokenCdDown { string dir; }
struct TokenCdUp {}
struct TokenLs {}
struct TokenDir { string dir; }
struct TokenFile { size_t size; }
alias Token = SumType!(TokenCdDown, TokenCdUp, TokenLs, TokenDir, TokenFile);

Token[] tokenize(string input)
{
    return
        input
        .lineSplitter
        .map!((string line) {
                 if (line == "$ cd ..")                                 return TokenCdUp().to!Token;
            else if (line == "$ ls")                                    return TokenLs().to!Token;
            else if (auto captures = line.matchFirst(`^\$ cd (.+)$`))   return TokenCdDown(captures[1]).to!Token;
            else if (auto captures = line.matchFirst(`^dir (.+)$`))     return TokenDir(captures[1]).to!Token;
            else if (auto captures = line.matchFirst(`^(\d+) .+$`))     return TokenFile(captures[1].to!size_t).to!Token;
            else assert(false, "No match for line: " ~ line);
        })
        .array;
}

@("tokenize")
unittest
{
    expect(tokenize(
`$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
`
    )).toEqual([
        TokenCdDown("/").to!Token,
        TokenLs().to!Token,
        TokenDir("a").to!Token,
        TokenFile(14_848_514).to!Token,
        TokenFile(8_504_156).to!Token,
        TokenDir("d").to!Token,
        TokenCdDown("a").to!Token,
        TokenLs().to!Token,
        TokenDir("e").to!Token,
        TokenFile(29_116).to!Token,
        TokenFile(2557).to!Token,
        TokenFile(62_596).to!Token,
        TokenCdDown("e").to!Token,
        TokenLs().to!Token,
        TokenFile(584).to!Token,
        TokenCdUp().to!Token,
        TokenCdUp().to!Token,
        TokenCdDown("d").to!Token,
        TokenLs().to!Token,
        TokenFile(4_060_174).to!Token,
        TokenFile(8_033_020).to!Token,
        TokenFile(5_626_152).to!Token,
        TokenFile(7_214_296).to!Token,
    ]);
}

struct Directory
{
    size_t[] fileSizes;
    Directory[string] subDirectories;

    ref Directory getDirAtPath(string[] path) return
    {
        if (path.length == 0) return this;
        return subDirectories[path[0]].getDirAtPath(path[1..$]);
    }

    void makeDirAtPath(string[] path)
    {
        if (path.length == 1) subDirectories[path[0]] = Directory();
        else subDirectories[path[0]].makeDirAtPath(path[1..$]);
    }
}

Directory interpret(Token[] tokens)
{
    Directory rootState;
    string[] currentDir;

    foreach (Token token; tokens)
    {
        token.match!(
            (TokenCdDown t) {
                if (t.dir == "/")
                {
                    currentDir = [];
                }
                else
                {
                    currentDir ~= t.dir;
                    rootState.makeDirAtPath(currentDir);
                }
            },
            (TokenCdUp t) { currentDir = currentDir[0 .. $-1]; },
            (TokenDir t) {/* Don't care */},
            (TokenFile t) {
                rootState.getDirAtPath(currentDir).fileSizes ~= t.size;
            },
            (TokenLs t) { /* Don't care */ },
        );
    }

    return rootState;
}

@("Interpret tokens and return resulting Dictionary")
unittest
{
    expect(interpret([
        TokenCdDown("/").to!Token,
        TokenLs().to!Token,
        TokenDir("a").to!Token,
        TokenFile(14_848_514).to!Token,
        TokenFile(8_504_156).to!Token,
        TokenDir("d").to!Token,
        TokenCdDown("a").to!Token,
        TokenLs().to!Token,
        TokenDir("e").to!Token,
        TokenFile(29_116).to!Token,
        TokenFile(2557).to!Token,
        TokenFile(62_596).to!Token,
        TokenCdDown("e").to!Token,
        TokenLs().to!Token,
        TokenFile(584).to!Token,
        TokenCdUp().to!Token,
        TokenCdUp().to!Token,
        TokenCdDown("d").to!Token,
        TokenLs().to!Token,
        TokenFile(4_060_174).to!Token,
        TokenFile(8_033_020).to!Token,
        TokenFile(5_626_152).to!Token,
        TokenFile(7_214_296).to!Token,
    ])).toEqual(Directory(
        [14_848_514, 8_504_156],
        [
            "a": Directory(
                [29_116, 2557, 62_596],
                [
                    "e": Directory(
                        [584],
                        null
                    )
                ],
            ),
            "d": Directory(
                [4_060_174, 8_033_020, 5_626_152, 7_214_296],
                null,
            ),
        ],
    ));
}

// // Returns the directory and the number of lines consumed in creating it.
// Tuple!(Directory, size_t) parseToDirectory(Token[] tokens)
// {
//     Directory result;
//     size_t tokensConsumed;

//     foreach (size_t i, Token token; tokens)
//     {
//         bool shouldReturnNow = false;

//         token.match!(
//             (TokenCdUp _) { shouldReturnNow = true; },
//             (TokenDir _) {},
//             (TokenFile t) { result.fileSizes ~= t.size; },
//             (TokenLs _) {/* dont care */},
//             (TokenCdDown t) {
//                 auto subResult = parseToDirectory(tokens[i+1 .. $]);
//                 result.subDirectories ~= subResult[0];
//                 tokensConsumed += subResult[1];
//             },
//         );

//         tokensConsumed++;

//         if (shouldReturnNow)
//             return tuple(result, tokensConsumed);
//     }

//     return tuple(result, tokensConsumed);
// }



size_t calcDirectoryFileSize(Directory dir)
{
    return
        dir.fileSizes.sum +
        dir.subDirectories.values.map!calcDirectoryFileSize.sum;
}

@("calcDirectoryFileSize of Example")
unittest
{
    // e
    expect(calcDirectoryFileSize(Directory(
        [584],
        null,
    ))).toEqual(584);

    // a
    expect(calcDirectoryFileSize(Directory(
            [29_116, 2557, 62_596],
            [
                "e": Directory(
                    [584],
                    null,
                )
            ],
    ))).toEqual(94_853);

    // root '/'
    expect(calcDirectoryFileSize(Directory(
        [14_848_514, 8_504_156],
        [
            "a": Directory(
                [29_116, 2557, 62_596],
                [
                    "e": Directory( //e
                        [584],
                        null,
                    )
                ],
            ),
            "d": Directory(
                [4_060_174, 8_033_020, 5_626_152, 7_214_296],
                null,
            ),
        ]
    ))).toEqual(48_381_165);
}

ReturnType!fun[] visitDirectories(alias fun)(Directory dir)
if (
    isSomeFunction!fun &&
    (Parameters!fun).length == 1 &&
    is(Parameters!fun[0] == Directory)
)
{
    return
        fun(dir) ~
        dir.subDirectories
            .values
            .map!((subDir) => visitDirectories!fun(subDir))
            .fold!((acc, e) => acc ~ e)(cast(ReturnType!fun[])[])
            .array;
}

size_t getSumOfDirectoriesLessThan(size_t threshold)(Directory dir)
{
    return
        dir
        .visitDirectories!(
            (Directory dir) {
                size_t dirSize = calcDirectoryFileSize(dir);
                return dirSize < threshold ? dirSize : 0;
            }
        )
        .sum;
}

@("Visit directories in tree and return the sum of those whose size is less than a threshold")
unittest
{
    expect(getSumOfDirectoriesLessThan!100_000(Directory(
        [14_848_514, 8_504_156],
        [
            "a": Directory(
                [29_116, 2557, 62_596],
                [
                    "e": Directory(
                        [584],
                        null
                    )
                ],
            ),
            "d": Directory(
                [4_060_174, 8_033_020, 5_626_152, 7_214_296],
                null,
            ),
        ],
    ))).toEqual(95_437);
}

size_t getSmallestDirectoryGreaterThan(size_t threshold, Directory dir)
{
    return
        dir
        .visitDirectories!calcDirectoryFileSize
        .filter!((size_t size) => size >= threshold)
        .minElement;
}

@("Visit directories in tree and return the size of the smallest one greater than a threshold")
unittest
{
    expect(getSmallestDirectoryGreaterThan(
        8_381_165,
        Directory(
            [14_848_514, 8_504_156],
            [
                "a": Directory(
                    [29_116, 2557, 62_596],
                    [
                        "e": Directory(
                            [584],
                            null
                        )
                    ],
                ),
                "d": Directory(
                    [4_060_174, 8_033_020, 5_626_152, 7_214_296],
                    null,
                ),
            ],
        ),
    )).toEqual(24_933_642);
}
