module advent_of_code_2022.day_2.day_2;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.string;


enum Moves : int
{
    rock = 1,
    paper,
    scissors,
}

enum Moves[char] letterToMovePart1 = [
    'A': Moves.rock,
    'B': Moves.paper,
    'C': Moves.scissors,
    'X': Moves.rock,
    'Y': Moves.paper,
    'Z': Moves.scissors,
];

enum Moves[char] letterToMovePart2 = [
    'A': Moves.rock,
    'B': Moves.paper,
    'C': Moves.scissors,
];

string day2Part1(string input)
{
    return input
        .chomp
        .split("\n")
        .map!((string line) => scoreForRound(letterToMovePart1[line[0]], letterToMovePart1[line[2]]))
        .sum
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day2Part1(
`A Y
B X
C Z
`
    )).toEqual("15");
}

string day2Part2(string input)
{
    return input
        .chomp
        .split("\n")
        .map!((string line) => scoreForRound(
            letterToMovePart2[line[0]],
            getNecessaryMovePart2(letterToMovePart2[line[0]], line[2])
        ))
        .sum
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day2Part2(
`A Y
B X
C Z
`
    )).toEqual("12");
}

Moves getNecessaryMovePart2(Moves opponentMove, char result)
{
    final switch (opponentMove)
    {
        case Moves.rock:
            return
                result == 'X' ?
                Moves.scissors :
                result == 'Y' ?
                Moves.rock :
                Moves.paper;

        case Moves.paper:
            return
                result == 'X' ?
                Moves.rock :
                result == 'Y' ?
                Moves.paper :
                Moves.scissors;

        case Moves.scissors:
            return
                result == 'X' ?
                Moves.paper :
                result == 'Y' ?
                Moves.scissors :
                Moves.rock;
    }
}

int scoreForRound(Moves opponentMove, Moves yourMove)
{
    int score;
    score += yourMove;

    if (opponentMove == Moves.rock && yourMove == Moves.scissors)
    {
        // you lost
    }
    else if (opponentMove == Moves.scissors && yourMove == Moves.rock)
    {
        // you win
        score += 6;
    }
    else
    {
        int result = yourMove - opponentMove;
        if (result > 0)
        {
            // you won
            score += 6;
        }
        else if (result < 0)
        {
            // you lost
        }
        else
        {
            // draw
            score += 3;
        }
    }

    return score;
}

@("Score per round")
unittest
{
    expect(scoreForRound(Moves.rock, Moves.paper)).toEqual(8);
    expect(scoreForRound(Moves.paper, Moves.rock)).toEqual(1);
    expect(scoreForRound(Moves.scissors, Moves.scissors)).toEqual(6);
}
