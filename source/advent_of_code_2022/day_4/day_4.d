module advent_of_code_2022.day_4.day_4;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.regex;


string day4Part1(string input)
{
    return
        input
        .parseInput
        .filter!isCompletelyOverlapping
        .walkLength
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day4Part1(
`2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
`
    )).toEqual("2");
}

string day4Part2(string input)
{
    return
        input
        .parseInput
        .filter!isPartiallyOverlapping
        .walkLength
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day4Part2(
`2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
`
    )).toEqual("4");
}

struct Assignment
{
    // both inclusive
    int start;
    int end;
}

Assignment[2][] parseInput(string input)
{
    return
        input
        .matchAll(`(\d+)-(\d+),(\d+)-(\d+)`)
        .map!((capture) => [
            Assignment(capture[1].to!int, capture[2].to!int),
            Assignment(capture[3].to!int, capture[4].to!int)
        ].to!(Assignment[2]))
        .array;
}

@("Parse input")
unittest
{
    expect(parseInput(
`2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
`
    )).toEqual([
        [Assignment(2, 4), Assignment(6, 8)],
        [Assignment(2, 3), Assignment(4, 5)],
        [Assignment(5, 7), Assignment(7, 9)],
        [Assignment(2, 8), Assignment(3, 7)],
        [Assignment(6, 6), Assignment(4, 6)],
        [Assignment(2, 6), Assignment(4, 8)],
    ]);
}

bool isCompletelyOverlapping(Assignment[2] pair)
{
    return
        (pair[0].start <= pair[1].start && pair[0].end >= pair[1].end) ||
        (pair[1].start <= pair[0].start && pair[1].end >= pair[0].end);
}

@("isCompletelyOverlapping")
unittest
{
    expect(isCompletelyOverlapping([Assignment(2, 4), Assignment(6, 8)])).toEqual(false);
    expect(isCompletelyOverlapping([Assignment(2, 3), Assignment(4, 5)])).toEqual(false);
    expect(isCompletelyOverlapping([Assignment(5, 7), Assignment(7, 9)])).toEqual(false);
    expect(isCompletelyOverlapping([Assignment(2, 8), Assignment(3, 7)])).toEqual(true);
    expect(isCompletelyOverlapping([Assignment(6, 6), Assignment(4, 6)])).toEqual(true);
    expect(isCompletelyOverlapping([Assignment(2, 6), Assignment(4, 8)])).toEqual(false);
}

bool isPartiallyOverlapping(Assignment[2] pair)
{
    Assignment first = pair[0];
    Assignment second = pair[1];
    return
        (first.end >= second.start && first.end <= second.end) ||
        (first.start >= second.start && first.start <= second.end) ||
        (second.end >= first.start && second.end <= first.end) ||
        (second.start >= first.start && second.start <= first.end);
}

@("isPartiallyOverlapping")
unittest
{
    expect(isPartiallyOverlapping([Assignment(2, 4), Assignment(6, 8)])).toEqual(false);
    expect(isPartiallyOverlapping([Assignment(2, 3), Assignment(4, 5)])).toEqual(false);
    expect(isPartiallyOverlapping([Assignment(5, 7), Assignment(7, 9)])).toEqual(true);
    expect(isPartiallyOverlapping([Assignment(2, 8), Assignment(3, 7)])).toEqual(true);
    expect(isPartiallyOverlapping([Assignment(6, 6), Assignment(4, 6)])).toEqual(true);
    expect(isPartiallyOverlapping([Assignment(2, 6), Assignment(4, 8)])).toEqual(true);
}
