module advent_of_code_2022.day_3.day_3;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.ascii;
import std.conv;
import std.range;
import std.string;


string day3Part1(string input)
{
    return
        input
        .parse
        .map!findMistake
        .map!getPriority
        .sum
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day3Part1(
`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
`
    )).toEqual("157");
}

string[2][] parse(string input)
{
    return input
        .lineSplitter()
        .map!((string line) => [line[0 .. $/2], line[$/2 .. $]].to!(string[2]))
        .array;
}

@("Parse")
unittest
{
    expect(parse(
`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
`
    )).toEqual([
        ["vJrwpWtwJgWr", "hcsFMMfFFhFp"],
        ["jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"]
    ]);
}

char findMistake(string[2] rucksack)
{
    return findAmong(rucksack[0], rucksack[1])[0];
}

@("Find mistake")
unittest
{
    expect(findMistake(["vJrwpWtwJgWr", "hcsFMMfFFhFp"])).toEqual('p');
    expect(findMistake(["jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"])).toEqual('L');
}

int getPriority(char c)
{
    return
        isLower(c) ?
        c - 'a' + 1 :
        c - 'A' + 27;
}

@("Get priority")
unittest
{
    expect(getPriority('p')).toEqual(16);
    expect(getPriority('L')).toEqual(38);
    expect(getPriority('P')).toEqual(42);
    expect(getPriority('v')).toEqual(22);
    expect(getPriority('t')).toEqual(20);
    expect(getPriority('s')).toEqual(19);
}

string day3Part2(string input)
{
    return
        input
        .lineSplitter
        .chunks(3)
        .map!array              // Take doesn't return a random access range.
        .map!getCommonItem
        .map!getPriority
        .sum
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day3Part2(
`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
`
    )).toEqual("70");
}

char getCommonItem(Range)(Range group)
if (isRandomAccessRange!Range)
in (group.length == 3)
{
    return group[0].findAmong(group[1]).findAmong(group[2])[0];
}

@("Get a groups only shared item")
unittest
{
    expect(getCommonItem([
        "vJrwpWtwJgWrhcsFMMfFFhFp",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
        "PmmdzqPrVvPwwTWBwg",
    ])).toEqual('r');

    expect(getCommonItem([
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
        "ttgJtRGJQctTZtZT",
        "CrZsJsPPZsGzwwsLwLmpwMDw",
    ])).toEqual('Z');
}
