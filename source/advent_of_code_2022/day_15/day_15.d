module advent_of_code_2022.day_15.day_15;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.regex;
import std.string;


version(unittest) enum string exampleInput =
`Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
`;

struct Vector2
{
    int x;
    int y;

    int manhattanDistanceTo(const Vector2 other) const
    {
        return
            abs(this.x - other.x) +     // x distance
            abs(this.y - other.y);      // y distance
    }
}

struct Sensor
{
    Vector2 position;
    Vector2 beacon;

    int range() const
    {
        return position.manhattanDistanceTo(beacon);
    }
}

string day15Part1(string input)
{
    return countImpossibleInRow(input, 2_000_000).to!string;
}

int countImpossibleInRow(string input, int y)
{
    Sensor[] sensors = parseInput(input);

    int minPossibleX = sensors.map!((Sensor s) => s.position.x - s.range).minElement;
    int maxPossibleX = sensors.map!((Sensor s) => s.position.x + s.range).maxElement;

    bool[Vector2] coveredBySensors;

    foreach (Sensor sensor; sensors)
    {
        // If the sensor's range overlaps this row, it can block certain cells.
        if (y >= sensor.position.y - sensor.range() && y <= sensor.position.y + sensor.range())
        {
            // TODO: no need to investigate the whole row
            foreach (int x; minPossibleX .. maxPossibleX + 1)
            {
                Vector2 currentPos = Vector2(x, y);
                if (currentPos.manhattanDistanceTo(sensor.position) <= sensor.range)
                {
                    coveredBySensors[currentPos] = true;
                }
            }
        }
    }

    foreach (Sensor sensor; sensors)
    {
        coveredBySensors[sensor.beacon] = false;
    }

    return coveredBySensors.values.filter!(e => e).array.length.to!int;
}

@("Example Part 1")
unittest
{
    expect(countImpossibleInRow(exampleInput, 10)).toEqual(26);
}

string day15Part2(string input)
{
    Vector2 beaconPosition = getOnlyPossiblePosition(input, 0, 4_000_000);
    return (beaconPosition.x.to!size_t * 4_000_000UL + beaconPosition.y.to!size_t).to!string;
}

Vector2 getOnlyPossiblePosition(string input, int minXY, int maxXY)
{
    Sensor[] sensors = parseInput(input);

    for (int y = minXY; y <= maxXY; y++)
    {
        int x = minXY;

        xLoop:
        while (x <= maxXY)
        {
            Vector2 currentPos = Vector2(x, y);

            foreach (Sensor sensor; sensors)
            {
                int distanceToSensor = currentPos.manhattanDistanceTo(sensor.position);
                if (distanceToSensor <= sensor.range())
                {
                    // How much can we increment by?
                    // 1 is the safe option but it's really slow. We can do
                    // better by skipping regions we know for sure are covered
                    // by the current sensor.
                    int yDistance = abs(currentPos.y - sensor.position.y);
                    int xDistance = sensor.position.x - currentPos.x;
                    int maxSensorWidth = (sensor.range() * 2) + 1;
                    int taperAtThisY = yDistance * 2;
                    int widthAtThisY = maxSensorWidth - taperAtThisY;
                    int distanceAlreadyIntoRange = ((widthAtThisY - 1) / 2) - xDistance;
                    int xStepsToIncrement = widthAtThisY - distanceAlreadyIntoRange;

                    x += xStepsToIncrement;
                    continue xLoop;
                }
            }

            return currentPos;
        }
    }

    assert(false, "No cell is out of range of every sensor");
}

@("Example Part 2")
unittest
{
    expect(getOnlyPossiblePosition(exampleInput, 0, 20)).toEqual(Vector2(14, 11));
}

Sensor[] parseInput(string input)
{
    return
        input
        .chomp
        .lineSplitter
        .map!(
            (string line) {
                auto captures = line.matchFirst(`^Sensor at x=([-\d]+), y=([-\d]+): closest beacon is at x=([-\d]+), y=([-\d]+)$`);
                return Sensor(
                    Vector2(captures[1].to!int, captures[2].to!int),
                    Vector2(captures[3].to!int, captures[4].to!int)
                );
            }
        )
        .array;
}


@("Parse example input")
unittest
{
    expect(parseInput(exampleInput)).toEqual([
        Sensor(Vector2(2, 18), Vector2(-2, 15)),
        Sensor(Vector2(9, 16), Vector2(10, 16)),
        Sensor(Vector2(13, 2), Vector2(15, 3)),
        Sensor(Vector2(12, 14), Vector2(10, 16)),
        Sensor(Vector2(10, 20), Vector2(10, 16)),
        Sensor(Vector2(14, 17), Vector2(10, 16)),
        Sensor(Vector2(8, 7), Vector2(2, 10)),
        Sensor(Vector2(2, 0), Vector2(2, 10)),
        Sensor(Vector2(0, 11), Vector2(2, 10)),
        Sensor(Vector2(20, 14), Vector2(25, 17)),
        Sensor(Vector2(17, 20), Vector2(21, 22)),
        Sensor(Vector2(16, 7), Vector2(15, 3)),
        Sensor(Vector2(14, 3), Vector2(15, 3)),
        Sensor(Vector2(20, 1), Vector2(15, 3)),
    ]);
}
