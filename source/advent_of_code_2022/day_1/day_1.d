module advent_of_code_2022.day_1.day_1;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.string;


string day1Part1(string input)
{
    return
        parseInput(input)
        .map!((int[] inventory) => inventory.sum)
        .maxElement
        .to!string;
}

@("Example Part 1")
unittest
{
    expect(day1Part1(
`1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
`
    )).toEqual("24000");
}

string day1Part2(string input)
{
    enum size_t numberOfTopElves = 3;

    int[] elfInventoryTotals =
        parseInput(input)
        .map!((int[] inventory) => inventory.sum)
        .array;

    partialSort!((a, b) => a > b)(elfInventoryTotals, numberOfTopElves);

    return
        elfInventoryTotals
        .take(numberOfTopElves)
        .sum
        .to!string;
}

@("Example Part 2")
unittest
{
    expect(day1Part2(
`1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
`
    )).toEqual("45000");
}

int[][] parseInput(string input)
{
    return input
        .chomp
        .split("\n\n")
        .map!(
            (string elfInventory) => elfInventory
                .split("\n")
                .map!(
                    (string cals) => parse!int(cals)
                ).array
        ).array;
}

@("Parse Example")
unittest
{
    expect(parseInput(
`1000
2000
3000

4000

5000
6000

7000
8000
9000

10000`
    )).toEqual([
        [1000, 2000, 3000],
        [4000],
        [5000, 6000],
        [7000, 8000, 9000],
        [10_000],
    ]);
}
