module advent_of_code_2022.day_11.day_11;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.bigint;
import std.conv;
import std.range;
import std.regex;
import std.string;
import std.sumtype;
import std.typecons;


version(unittest)
{
    import std.path;
    enum string exampleInput = import(__FILE__.pathSplitter.array[1 .. $-1].join("/").buildPath("example.txt"));
}


string day11Part1(string input)
{
    Monkey[] monkeyState = input.parseInput();

    foreach (i; 0..20)
        iterateRound(monkeyState, Yes.easilyRelieved);

    return
        monkeyState
        .map!(m => m.numInspections)
        .array
        .sort
        .retro
        .take(2)
        .fold!((acc, e) => acc * e)(1UL)
        .to!string;
}

@("Part 1 example")
unittest
{
    expect(day11Part1(exampleInput)).toEqual("10605");
}

string day11Part2(string input)
{
    Monkey[] monkeyState = input.parseInput();

    foreach (i; 0..10_000)
        iterateRound(monkeyState, No.easilyRelieved);

    return
        monkeyState
        .map!(m => m.numInspections)
        .array
        .sort
        .retro
        .take(2)
        .fold!((acc, e) => acc * e)(1UL)
        .to!string;
}

@("Part 2 example")
unittest
{
    expect(day11Part2(exampleInput)).toEqual("2713310158");
}

struct OpAdd { int addend; }
struct OpMultiply { int factor; }
struct OpSquare {}
alias Operation = SumType!(OpAdd, OpMultiply, OpSquare);

struct Monkey
{
    size_t[] items;
    Operation operation;
    int divisibilityTest;
    size_t trueTarget;
    size_t falseTarget;
    size_t numInspections;

    size_t operateOnWorryLevel(size_t worryLevel) const
    {
        return operation.match!(
            (OpAdd add) => worryLevel + add.addend,
            (OpMultiply mul) => worryLevel * mul.factor,
            (OpSquare _sq) => worryLevel * worryLevel,
        );
    }
}

Monkey parseMonkey(string input)
{
    string[] lines = input.chomp.lineSplitter.array;
    assert(lines.length == 6, "Monkey is invalid!");

    size_t[] startingItems =
        lines[1]
        [lines[1].indexOf(':') + 1 .. $]
        .split(",")
        .map!((string num) => chompPrefix(num, " "))
        .map!(to!size_t)
        .array;

    auto match = lines[2].matchFirst(`Operation: new = old ([+*]) (\w+)$`);
    string operator = match[1];
    string rhs = match[2];

    Operation operation;
    if (operator == "*" && rhs == "old")
        operation = OpSquare();
    else if (operator == "*")
        operation = OpMultiply(rhs.to!int);
    else if (operator == "+")
        operation = OpAdd(rhs.to!int);
    else
        assert(false, "unreachable: operator is " ~ operator ~ " and rhs is " ~ rhs);

    int divisibilityTest = lines[3].matchFirst(`\d+`)[0].to!int;
    int trueTarget = lines[4].matchFirst(`\d+`)[0].to!int;
    int falseTarget = lines[5].matchFirst(`\d+`)[0].to!int;

    return Monkey(
        startingItems,
        operation,
        divisibilityTest,
        trueTarget,
        falseTarget,
    );
}

@("Parse Monkey 0 from the example")
unittest
{
    expect(parseMonkey(
`Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3`
    )).toEqual(
        Monkey([79, 98], OpMultiply(19).to!Operation, 23, 2, 3)
    );
}

Monkey[] parseInput(string input)
{
    return
        input
        .split("\n\n")
        .map!chomp
        .map!parseMonkey
        .array;
}

@("Parse the example")
unittest
{
    expect(parseInput(exampleInput)).toEqual([
        Monkey([79, 98], OpMultiply(19).to!Operation, 23, 2, 3),
        Monkey([54, 65, 75, 74], OpAdd(6).to!Operation, 19, 2, 0),
        Monkey([79, 60, 97], OpSquare().to!Operation, 13, 1, 3),
        Monkey([74], OpAdd(3).to!Operation, 17, 0, 1),
    ]);
}

void iterateRound(Monkey[] monkeyState, Flag!"easilyRelieved" easilyRelieved)
{
    size_t worryOverflow = monkeyState.map!((Monkey m) => m.divisibilityTest).fold!"a * b";

    foreach (ref Monkey monkey; monkeyState)
    {
        monkey.numInspections += monkey.items.length;

        foreach (size_t item; monkey.items)
        {
            size_t worryLevel = (monkey.operateOnWorryLevel(item) / (easilyRelieved ? 3 : 1)) % worryOverflow;
            bool test = worryLevel % monkey.divisibilityTest == 0;
            if (test)
                monkeyState[monkey.trueTarget].items ~= worryLevel;
            else
                monkeyState[monkey.falseTarget].items ~= worryLevel;
        }

        monkey.items = [];
    }
}

@("Iterate rounds and count inspections from the example")
unittest
{
    Monkey[] monkeyState = [
        Monkey([79, 98], OpMultiply(19).to!Operation, 23, 2, 3),
        Monkey([54, 65, 75, 74], OpAdd(6).to!Operation, 19, 2, 0),
        Monkey([79, 60, 97], OpSquare().to!Operation, 13, 1, 3),
        Monkey([74], OpAdd(3).to!Operation, 17, 0, 1),
    ];

    // Round 1
    iterateRound(monkeyState, Yes.easilyRelieved);
    expect(monkeyState).toEqual([
        Monkey([20, 23, 27, 26], OpMultiply(19).to!Operation, 23, 2, 3, 2),
        Monkey([2080, 25, 167, 207, 401, 1046], OpAdd(6).to!Operation, 19, 2, 0, 4),
        Monkey([], OpSquare().to!Operation, 13, 1, 3, 3),
        Monkey([], OpAdd(3).to!Operation, 17, 0, 1, 5),
    ]);

    // Round 2
    iterateRound(monkeyState, Yes.easilyRelieved);
    expect(monkeyState).toEqual([
        Monkey([695, 10, 71, 135, 350], OpMultiply(19).to!Operation, 23, 2, 3, 6),
        Monkey([43, 49, 58, 55, 362], OpAdd(6).to!Operation, 19, 2, 0, 10),
        Monkey([], OpSquare().to!Operation, 13, 1, 3, 4),
        Monkey([], OpAdd(3).to!Operation, 17, 0, 1, 10),
    ]);
}

@("Iterate 20 rounds")
unittest
{
    Monkey[] monkeyState = [
        Monkey([79, 98], OpMultiply(19).to!Operation, 23, 2, 3),
        Monkey([54, 65, 75, 74], OpAdd(6).to!Operation, 19, 2, 0),
        Monkey([79, 60, 97], OpSquare().to!Operation, 13, 1, 3),
        Monkey([74], OpAdd(3).to!Operation, 17, 0, 1),
    ];

    foreach (int i; 0..20)
    {
        iterateRound(monkeyState, Yes.easilyRelieved);
    }

    expect(monkeyState).toEqual([
        Monkey([10, 12, 14, 26, 34], OpMultiply(19).to!Operation, 23, 2, 3, 101),
        Monkey([245, 93, 53, 199, 115], OpAdd(6).to!Operation, 19, 2, 0, 95),
        Monkey([], OpSquare().to!Operation, 13, 1, 3, 7),
        Monkey([], OpAdd(3).to!Operation, 17, 0, 1, 105),
    ]);
}
