module advent_of_code_2022.app;

import advent_of_code_2022.day_test;
import std.array;
import std.conv;
import std.file;
import std.path;
import std.stdio;
import std.string;


enum string[] dayPackages = () {
	return
		import("days.txt")
		.chomp
		.split;
}();

static foreach (string dayPackage; dayPackages)
{
	mixin("import advent_of_code_2022." ~ dayPackage ~ "." ~ dayPackage ~ ";");
}

static immutable string function(string)[string][string] dayPartFunctions;

shared static this() {
	static foreach (string dayPackage; dayPackages)
	{{
		enum string dayNumber = dayPackage[4..$];
		enum string day1FnName = "&day" ~ dayNumber ~ "Part1";
		enum string day2FnName = "&day" ~ dayNumber ~ "Part2";

		static if (__traits(compiles, mixin(day1FnName)))
		{
			dayPartFunctions[dayNumber]["1"] = mixin(day1FnName);
		}

		static if (__traits(compiles, mixin(day2FnName)))
		{
			dayPartFunctions[dayNumber]["2"] = mixin(day2FnName);
		}
	}}
}

int main(string[] args)
{
	if (args.length != 4)
	{
		writeln(
			"Program must be called with three arguments: the day number, the part number, and the source file.\n" ~
			"Example:\n" ~
			"dub run -- 17 2 source/advent_of_code_2022/day_17/part_2.txt"
		);

		return 1;
	}

	string dayName = args[1];
	string partName = args[2];
	string inputFilePath = args[3];

	if (dayName !in dayPartFunctions || partName !in dayPartFunctions[dayName])
	{
		writeln("No such day/part: Day " ~ dayName ~ " Part " ~ partName);
		return 1;
	}

	if (!exists(inputFilePath) || !isFile(inputFilePath))
	{
		writeln("File at path \"", inputFilePath, "\" not found");
		return 1;
	}

	string source = readText(inputFilePath);
	string result = dayPartFunctions[dayName][partName](source);
	writeln(result);

	return 0;
}
