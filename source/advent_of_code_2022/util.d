module advent_of_code_2022.util;

T inspect(alias transform = x => x, T)(T t) { import std.stdio : writeln; writeln(transform(t)); return t; }
