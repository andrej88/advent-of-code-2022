module advent_of_code_2022.day_5.day_5;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.regex;
import std.string;
import std.typecons;


string day5Part1(string input)
{
    auto parseResult = parseInput(input);
    char[][] state = parseResult[0];
    Instruction[] instrs = parseResult[1];

    foreach (Instruction instr; instrs)
    {
        executeInstructionPart1(state, instr);
    }

    string result;
    foreach (char[] stack; state)
    {
        result ~= stack[$ - 1];
    }

    return result;
}

@("Example Part 1")
unittest
{
    expect(day5Part1(
`    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`
    )).toEqual("CMZ");
}

string day5Part2(string input)
{
    auto parseResult = parseInput(input);
    char[][] state = parseResult[0];
    Instruction[] instrs = parseResult[1];

    foreach (Instruction instr; instrs)
    {
        executeInstructionPart2(state, instr);
    }

    string result;
    foreach (char[] stack; state)
    {
        result ~= stack[$ - 1];
    }

    return result;
}

@("Example Part 2")
unittest
{
    expect(day5Part2(
`    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`
    )).toEqual("MCD");
}


Tuple!(char[][], Instruction[]) parseInput(string input)
{
    string[] inputSplit = input.split("\n\n");
    assert(inputSplit.length == 2);
    return tuple(
        parseState(inputSplit[0]),
        parseInstructions(inputSplit[1]),
    );
}

@("Parse example input")
unittest
{
    expect(parseInput(
`    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`
    )).toEqual(tuple(
        [
            ['Z', 'N'],
            ['M', 'C', 'D'],
            ['P'],
        ],
        [
            Instruction(1, 2 - 1, 1 - 1),
            Instruction(3, 1 - 1, 3 - 1),
            Instruction(2, 2 - 1, 1 - 1),
            Instruction(1, 1 - 1, 2 - 1),
        ]
    ));
}

char[][] parseState(string input)
{
    return
        input
        .lineSplitter
        .array
        .dropBackOne
        .map!((string line) => chunks(line, 4))
        .map!(chunkedLine =>
            chunkedLine
            .map!(crate => crate.array[1].to!char)
            .array
        )
        .fold!((char[][] result, char[] line) {
            // Could probably be done more functionally
            // using std.range.indexed and whatnot
            foreach (size_t i, char c; line)
            {
                if (c == ' ') continue;
                if (result.length <= i)
                    result.length = i + 1;
                result[i] ~= c;
            }
            return result;
        })(cast(char[][])[])
        .map!retro
        .map!array
        .array
        .to!(char[][]); // idk. Without this it insists on being a dchar[][], just cos of the retro?
}

@("Parse example stack state")
unittest
{
    // Reversed each stack so can later push/pop from the ends more easily
    expect(parseState(
`    [D]
[N] [C]
[Z] [M] [P]
 1   2   3
`
    )).toEqual([
        ['Z', 'N'],
        ['M', 'C', 'D'],
        ['P'],
    ]);
}

struct Instruction
{
    int count;
    int from;
    int to;
}

Instruction[] parseInstructions(string input)
{
    return
        input
        .matchAll(`move (\d+) from (\d+) to (\d+)`)
        .map!(capture => Instruction(
            capture[1].to!int,
            capture[2].to!int - 1,
            capture[3].to!int - 1,
        ))
        .array;
}

@("Parse example instructions")
unittest
{
    // Also convert to 0-based indices while we're at it

    expect(parseInstructions(
`move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`
    )).toEqual([
        Instruction(1, 2 - 1, 1 - 1),
        Instruction(3, 1 - 1, 3 - 1),
        Instruction(2, 2 - 1, 1 - 1),
        Instruction(1, 1 - 1, 2 - 1),
    ]);
}

void executeInstructionPart1(char[][] state, Instruction instr)
{
    char[] toMove = state[instr.from][$ - instr.count .. $];
    state[instr.from].length -= instr.count;
    state[instr.to] ~= toMove.retro.array.to!(char[]);
}

@("Execute an instruction to move one crate (Part 1)")
unittest
{
    char[][] state = [
        ['Z', 'N'],
        ['M', 'C', 'D'],
        ['P'],
    ];

    executeInstructionPart1(state, Instruction(1, 2 - 1, 1 - 1));

    expect(state).toEqual([
        ['Z', 'N', 'D'],
        ['M', 'C'],
        ['P'],
    ]);
}

@("Execute an instruction to move multiple crates (Part 1)")
unittest
{
    char[][] state = [
        ['Z', 'N', 'D'],
        ['M', 'C'],
        ['P'],
    ];

    executeInstructionPart1(state, Instruction(3, 1 - 1, 3 - 1));

    expect(state).toEqual([
        [],
        ['M', 'C'],
        ['P', 'D', 'N', 'Z'],
    ]);
}

void executeInstructionPart2(char[][] state, Instruction instr)
{
    char[] toMove = state[instr.from][$ - instr.count .. $];
    state[instr.from].length -= instr.count;
    state[instr.to] ~= toMove;
}

@("Execute an instruction to move one crate (Part 2)")
unittest
{
    char[][] state = [
        ['Z', 'N'],
        ['M', 'C', 'D'],
        ['P'],
    ];

    executeInstructionPart2(state, Instruction(1, 2 - 1, 1 - 1));

    expect(state).toEqual([
        ['Z', 'N', 'D'],
        ['M', 'C'],
        ['P'],
    ]);
}

@("Execute an instruction to move multiple crates (Part 2)")
unittest
{
    char[][] state = [
        ['Z', 'N', 'D'],
        ['M', 'C'],
        ['P'],
    ];

    executeInstructionPart2(state, Instruction(3, 1 - 1, 3 - 1));

    expect(state).toEqual([
        [],
        ['M', 'C'],
        ['P', 'Z', 'N', 'D'],
    ]);
}
