module advent_of_code_2022.day_6.day_6;

import exceeds_expectations;
import std.algorithm;
import std.conv;
import std.range;


string day6Part1(string input)
{
    size_t i;
    for (i = 4; i < input.length; i++)
    {
        if (!containsDuplicates(input[i - 4 .. i])) break;
    }

    return i.to!string;
}

@("Examples Part 1")
unittest
{
    expect(day6Part1("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).toEqual("7");
    expect(day6Part1("bvwbjplbgvbhsrlpgdmjqwftvncz")).toEqual("5");
    expect(day6Part1("nppdvjthqldpwncqszvftbrmjlhg")).toEqual("6");
    expect(day6Part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).toEqual("10");
    expect(day6Part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).toEqual("11");
}

bool containsDuplicates(R)(R range)
{
    ElementType!R[] seenSoFar;
    foreach (item; range)
    {
        if (canFind(seenSoFar, item)) return true;
        seenSoFar ~= item;
    }

    return false;
}

@("Does the range contain duplicates")
unittest
{
    expect(containsDuplicates([1, 2, 65, 3, 5])).toEqual(false);
    expect(containsDuplicates([1, 2, 65, 3, 2])).toEqual(true);
}

string day6Part2(string input)
{
    size_t i;
    for (i = 14; i < input.length; i++)
    {
        if (!containsDuplicates(input[i - 14 .. i])) break;
    }

    return i.to!string;
}

@("Examples Part 2")
unittest
{
    expect(day6Part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).toEqual("19");
    expect(day6Part2("bvwbjplbgvbhsrlpgdmjqwftvncz")).toEqual("23");
    expect(day6Part2("nppdvjthqldpwncqszvftbrmjlhg")).toEqual("23");
    expect(day6Part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).toEqual("29");
    expect(day6Part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).toEqual("26");
}
