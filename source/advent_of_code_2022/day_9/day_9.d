module advent_of_code_2022.day_9.day_9;

import exceeds_expectations;
import std.array;
import std.conv;
import std.math;
import std.range;
import std.string;


string day9Part1(string input)
{
    return simulateTailPath(input, 1).length.to!string;
}

@("Example Part 1")
unittest
{
    expect(day9Part1(
`R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
`
    )).toEqual("13");
}

string day9Part2(string input)
{
    return simulateTailPath(input, 9).length.to!string;
}

@("Example Part 2")
unittest
{
    expect(day9Part2(
`R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
`
    )).toEqual("1");
}

@("Big example Part 2")
unittest
{
    expect(day9Part2(
`R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
`
    )).toEqual("36");
}

struct Position
{
    int x;
    int y;
}

bool[Position] simulateTailPath(string ropeMovement, uint numTails)
{
    bool[Position] visitedByTail;
    Position headPos = Position(0, 0);
    Position[] tailPos = Position(0, 0).repeat(numTails).array;

    foreach (string line; ropeMovement.chomp.lineSplitter)
    {
        string dir = line[0..1];
        uint distance = line[2..$].to!uint;

        foreach (uint step; 0 .. distance)
        {
            int headXDiff =
                dir == "L" ?
                -1 :
                dir == "R" ?
                1 :
                0;

            int headYDiff =
                dir == "D" ?
                -1 :
                dir == "U" ?
                1 :
                0;

            headPos.x += headXDiff;
            headPos.y += headYDiff;

            foreach (uint tailIndex; 0 .. numTails)
            {
                Position previousSegmentPos =
                    tailIndex == 0 ?
                    headPos :
                    tailPos[tailIndex - 1];

                int xDiff = previousSegmentPos.x - tailPos[tailIndex].x;
                int yDiff = previousSegmentPos.y - tailPos[tailIndex].y;

                if (abs(xDiff) > 1 || abs(yDiff) > 1)
                {
                    if (xDiff == 0)
                    {
                             if (yDiff < 1) tailPos[tailIndex].y -= 1;
                        else if (yDiff > 1) tailPos[tailIndex].y += 1;
                        else assert(false, "unreachable");
                    }
                    else if (yDiff == 0)
                    {
                             if (xDiff < 1) tailPos[tailIndex].x -= 1;
                        else if (xDiff > 1) tailPos[tailIndex].x += 1;
                        else assert(false, "unreachable");
                    }
                    else
                    {
                        // Both are not zero, and at least one is greater than one.
                        // It does not matter which, we move in the same diagonal direction regardless.

                        if (xDiff < 0 && yDiff < 0)
                        {
                            tailPos[tailIndex].x -= 1;
                            tailPos[tailIndex].y -= 1;
                        }
                        else if (xDiff < 0 && yDiff > 0)
                        {
                            tailPos[tailIndex].x -= 1;
                            tailPos[tailIndex].y += 1;
                        }
                        else if (xDiff > 0 && yDiff > 0)
                        {
                            tailPos[tailIndex].x += 1;
                            tailPos[tailIndex].y += 1;
                        }
                        else if (xDiff > 0 && yDiff < 0)
                        {
                            tailPos[tailIndex].x += 1;
                            tailPos[tailIndex].y -= 1;
                        }
                        else assert(false, "unreachable");
                    }
                }
            }

            visitedByTail[tailPos[$ - 1]] = true;
        }

        // debug {
        //     string diagram;
        //     foreach_reverse (int y; -10..10)
        //     {
        //         foreach (int x; -10..10)
        //         {
        //             if (Position(x, y) == headPos)
        //             {
        //                 diagram ~= "H";
        //             }
        //             else
        //             {
        //                 bool hasTail;
        //                 foreach (size_t tailIdx, Position pos; tailPos)
        //                 {
        //                     if (Position(x, y) == pos)
        //                     {
        //                         diagram ~= (tailIdx + 1).to!string;
        //                         hasTail = true;
        //                         break;
        //                     }
        //                 }

        //                 if (!hasTail) diagram ~= ".";
        //             }
        //         }

        //         diagram ~= "\n";
        //     }

        //     import std.stdio : writeln;
        //     try { writeln(diagram); } catch (Exception) {}
        // }
    }

    return visitedByTail;
}
