module advent_of_code_2022.day_12.day_12;

import exceeds_expectations;
import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.path;
import std.range;
import std.string;
import std.typecons;


version(unittest) enum string exampleInput = import(__FILE__.pathSplitter.array[1 .. $-1].join("/").buildPath("example.txt"));

struct Position { size_t x; size_t y; }


string day12Part1(string input)
{
    Tuple!(ubyte[][], Position, Position) parsedInput = parseInput(input);
    ubyte[][] heightmap = parsedInput[0];
    Position start = parsedInput[1];
    Position end = parsedInput[2];


    size_t[Position] shortestPathToEachPosition;
    // Subtract 1 because the problem wants # of steps, but the function returns
    // total squares visited.

    const(Position[]) path = getShortestPath(
        heightmap,
        start,
        (Position p) => p == end,
        [],
        size_t.max,
        shortestPathToEachPosition,
        (Position a, Position b) => manhattanDistance(a, end) < manhattanDistance(b, end),
        &isNeighbourAtMostOneHigher,
    );

    printCurrentMap(heightmap, path);

    return (path.length - 1).to!string;
}

@("Example part 1")
unittest
{
    expect(day12Part1(exampleInput)).toEqual("31");
}

string day12Part2(string input)
{
    Tuple!(ubyte[][], Position, Position) parsedInput = parseInput(input);
    ubyte[][] heightmap = parsedInput[0];
    Position start = parsedInput[2]; // start from the end


    size_t[Position] shortestPathToEachPosition;
    const(Position[]) path = getShortestPath(
        heightmap,
        start,
        (Position p) => heightmap[p.y][p.x] == 0,
        [],
        size_t.max,
        shortestPathToEachPosition,
        (Position a, Position b) => manhattanDistance(a, start) > manhattanDistance(b, start),  // Try to move away from the start
        &isNeighbourAtLeastOneLower,
    );

    printCurrentMap(heightmap, path);

    return (path.length - 1).to!string;
}

@("Example part 2")
unittest
{
    expect(day12Part2(exampleInput)).toEqual("29");
}

Tuple!(ubyte[][], Position, Position) parseInput(string input)
out(result) {
    const ubyte[][] heightmap = result[0];
    immutable size_t lineLength = heightmap[0].length;
    heightmap.all!(line => line.length == lineLength);
}
do {
    Position start;
    Position end;

    ubyte[][] heightmap =
        input
        .chomp
        .lineSplitter
        .enumerate
        .map!(
            (entry) {
                size_t y = entry[0];
                string line = entry[1];

                return line.enumerate.map!(
                    (entry) {
                        size_t x = entry[0];
                        char height = entry[1].to!char;
                        if (height == 'S')
                        {
                            start.x = x;
                            start.y = y;
                            return ubyte(0);
                        }
                        else if (height == 'E')
                        {
                            end.x = x;
                            end.y = y;
                            return ubyte('z' - 'a');
                        }
                        else return (height - 'a').to!ubyte;
                    }
                ).array;
            }
        ).array;

    return tuple(heightmap, start, end);
}

@("Parse example input")
unittest
{
    expect(parseInput(exampleInput)).toEqual(tuple(
        [
            [ 0,  0,  1, 16, 15, 14, 13, 12],
            [ 0,  1,  2, 17, 24, 23, 23, 11],
            [ 0,  2,  2, 18, 25, 25, 23, 10],
            [ 0,  2,  2, 19, 20, 21, 22,  9],
            [ 0,  1,  3,  4,  5,  6,  7,  8],
        ],
        Position(0, 0),
        Position(5, 2),
    ));
}

int manhattanDistance(Position start, Position end)
{
    return
        abs(start.x.to!int - end.x.to!int) +    // x distance
        abs(start.y.to!int - end.y.to!int);     // y distance
}

bool isNeighbourAtMostOneHigher(const ubyte[][] heightmap, const Position from, const Position to) pure
{
    return heightmap[to.y][to.x] <= heightmap[from.y][from.x] + 1;
}

bool isNeighbourAtLeastOneLower(const ubyte[][] heightmap, const Position from, const Position to) pure
{
    return heightmap[to.y][to.x] + 1 >= heightmap[from.y][from.x];
}

/// This is essentially a depth-first search that safely avoids going in cycles,
/// hence the need for alreadyVisited. The function assumes the heightmap is not
/// empty and is not jagged (rows must all be the same length).
const(Position[]) getShortestPath(
    const ubyte[][] heightmap,
    const Position start,
    bool delegate(Position) checkIfEndReached,
    const Position[] alreadyVisited,
    size_t shortestPathFoundSoFar,
    ref size_t[Position] shortestPathToEachPosition,
    bool delegate(Position, Position) neighbourSorter,
    bool function(const ubyte[][] heightmap, const Position from, const Position to) isStepLegal,
)
{
    // If we reached the end, return the current path + the end
    if (checkIfEndReached(start)) return alreadyVisited ~ start;

    // If further searches can't beat the path we already found, consider this result invalid.
    if (alreadyVisited.length + 1 >= shortestPathFoundSoFar) return null;

    // If we already know there's a faster way to get here, consider this option invalid
    if (
        start in shortestPathToEachPosition &&
        shortestPathToEachPosition[start] <= alreadyVisited.length
    )
        return null;

    shortestPathToEachPosition[start] = alreadyVisited.length;


    // Otherwise, we need to recursively search onwards.

    // First we need to figure out the valid neighbours of this position.
    // A neighbour is valid if:
    // - it's not off the edge of the map (obviously), hence the checks for start.x & y
    // - its height is at most one higher than the current position
    // We do this for each of the four orthogonal directions

    Position[] neighbours;

    if (start.x > 0)
    {
        Position neighbour = Position(start.x - 1, start.y);
        if (
            isStepLegal(heightmap, start, neighbour) &&
            !alreadyVisited.canFind(neighbour)
        )
            neighbours ~= neighbour;
    }

    if (start.x < heightmap[0].length - 1)
    {
        Position neighbour = Position(start.x + 1, start.y);
        if (
            isStepLegal(heightmap, start, neighbour) &&
            !alreadyVisited.canFind(neighbour)
        )
            neighbours ~= neighbour;
    }

    if (start.y > 0)
    {
        Position neighbour = Position(start.x, start.y - 1);
        if (
            isStepLegal(heightmap, start, neighbour) &&
            !alreadyVisited.canFind(neighbour)
        )
            neighbours ~= neighbour;
    }

    if (start.y < heightmap.length - 1)
    {
        Position neighbour = Position(start.x, start.y + 1);
        if (
            isStepLegal(heightmap, start, neighbour) &&
            !alreadyVisited.canFind(neighbour)
        )
            neighbours ~= neighbour;
    }

    // If there are no neighbours we can visit, this path doesn't lead anywhere.
    // Return null to indicate that this was an invalid approach. Technically
    // this line is not necessary since null would be returned anyway, but I
    // think it helps to make it explicit.
    if (neighbours.length == 0) return null;

    // We want to be as quick as we can and use a heuristic to decide which
    // neighbours to try first. The heuristic is "manhattan distance to end".
    // This is an admissible heuristic because it's always <= the length of the
    // actual path. (https://artint.info/2e/html/ArtInt2e.Ch3.S6.html) In
    // practice, the speeds up Part 1 by a factor of 2. This could probably be
    // improved by adding a tiebreaker: prefer the position that is "sideways"
    // rather than "backwards" from the end compared to the actual current
    // position.
    auto sortedNeighbours = neighbours.sort!neighbourSorter;

    // Now that we know which neighbours to check, we recursively call this
    // function on each neighbour and return the best result.

    const(Position)[] bestNeighbourResult;
    foreach (Position neighbour; sortedNeighbours)
    {
        // This next line drastically slows down the program when run on problem.txt.
        // printCurrentMap(heightmap, alreadyVisited ~ start ~ neighbour);
        const(Position[]) currentNeighbourResult = getShortestPath(
            heightmap,
            neighbour,
            checkIfEndReached,
            alreadyVisited ~ start,
            shortestPathFoundSoFar,
            shortestPathToEachPosition,
            neighbourSorter,
            isStepLegal,
        );

        // If the choice was not valid, keep trying with the next neighbour.
        if (currentNeighbourResult is null) continue;

        // debug { import std.stdio : writeln; try { writeln("Found a solution with length ", currentNeighbourResult.length); } catch (Exception) {} }

        // Else the choice *was* valid, in which case we check if it was the
        // shortest choice so far.
        if (bestNeighbourResult is null || currentNeighbourResult.length < bestNeighbourResult.length) {
            bestNeighbourResult = currentNeighbourResult;

            if (bestNeighbourResult.length < shortestPathFoundSoFar)
                shortestPathFoundSoFar = bestNeighbourResult.length;
        }
    }

    // Return the shortest path to the end starting from this node, or null if
    // none of the neighbours gave valid paths (e.g. there was only one
    // neighbour and it was a dead end.)

    return bestNeighbourResult;
}

@("Get shortest gentle path in the example")
unittest
{
    ubyte[][] heightmap = [
        [ 0,  0,  1, 16, 15, 14, 13, 12],
        [ 0,  1,  2, 17, 24, 23, 23, 11],
        [ 0,  2,  2, 18, 25, 25, 23, 10],
        [ 0,  2,  2, 19, 20, 21, 22,  9],
        [ 0,  1,  3,  4,  5,  6,  7,  8],
    ];
    Position start = Position(0, 0);
    Position end = Position(5, 2);

    size_t[Position] shortestPathToEachPosition;
    const(Position[]) result = getShortestPath(
        heightmap,
        start,
        (Position p) => p == end,
        [],
        size_t.max,
        shortestPathToEachPosition,
        (Position a, Position b) => manhattanDistance(a, end) < manhattanDistance(b, end),
        &isNeighbourAtMostOneHigher,
    );
    expect(result).toSatisfy(p => p.length == 32);
}

void printCurrentMap(const ubyte[][] heightmap, const Position[] path)
{
    import std.stdio : write, writeln;
    import colorize;

    foreach (size_t y, const ubyte[] line; heightmap)
    {
        foreach (size_t x, ubyte c; line)
        {

            if (path.canFind(Position(x, y)))
            {
                write((c + 'a').to!char.to!string.color(mode.bold));
            }
            else
            {
                write((c + 'a').to!char);
            }
        }

        writeln();
    }

    writeln();
}
