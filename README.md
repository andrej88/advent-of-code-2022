Advent of Code 2022 solutions written in D.

Requires the D toolchain installed (at a minimum, dub and dmd).

- To run all unittests, do: `dub test`
- To run specific unittests, do: `dub test -- -i day_1` where `day_1` is the module or test name pattern to include.
- To run unittests and automatically re-run when the source changes, do: `./scripts/watch_unit_tests.d`.
- To do that for only specific modules/test names, do: `./scripts/watch_unit_tests.d -i day_1`
- To run a specific day's solution given an input file, do: `dub run -- day_number part_number path_to_file`.
  - For example: `dub run -- 3 2 source/advent_of_code_2022/day_3/problem.txt` will run Day 3 Part 2 with the given input.

On macOS and D release 2.101, there is an issue with the linker not liking the alignment of some stuff. The workaround for now is to add `MACOSX_DEPLOYMENT_TARGET=11` before any of those commands, e.g. `MACOSX_DEPLOYMENT_TARGET=11 ./scripts/watch_unit_tests.d`
